﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Kosmita:IMężczyzna,IKosmita
    {
        private string imie;

	public string Imie
	{
		get { return imie;}
		set { imie = value;}
	}

        private string nazwisko;

	public string Nazwisko
	{
		get { return nazwisko;}
		set { nazwisko = value;}
	}
	
        public Kosmita()
        {
            this.imie = "Kosmita";
            this.nazwisko = "Kowalski";
        }

        public string ImAMen()
        {
            return "Jestem mężczyzną";
        }

        public string Hi()
        {
            return "Nazywam się: " + imie + " " + nazwisko;
        }

        string IMężczyzna.ImAMen()
        {
            return "Jestem mężczyzną";
        }

        string ICzłowiek.Hi()
        {
            return "Jestem człowiekiem i nazwyam się " + imie + " " + nazwisko;
        }


        public string ImAUFO()
        {
            return "Jestem ufo!";
        }

        string IKosmita.Hi()
        {
            return "Jestem kosmitą i nazywam się " + imie + " " + nazwisko;
        }

        string IKosmita.ImAUFO()
        {
            return "Jestem ufo!";
        }
    }
}
