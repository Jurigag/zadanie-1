﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Mężczyzna:IMężczyzna
    {
        private string imie;

        public string Imie
        {
            get { return imie; }
            set { imie = value; }
        }

        private string nazwisko;

        public string Nazwisko
        {
            get { return nazwisko; }
            set { nazwisko = value; }
        }

        public Mężczyzna()
        {
            this.imie = "Jan";
            this.nazwisko = "Kowalski";
        }

        public string ImAMen()
        {
            return "Jestem mężczyzną !";
        }

        public string Hi()
        {
            return "Nazywam się: " + imie + " " + nazwisko;
        }
    }
}
