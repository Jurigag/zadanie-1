﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        public static IList<ICzłowiek> factoryMethod()
        {
            Kobieta k = new Kobieta();
            Mężczyzna m = new Mężczyzna();
            List<ICzłowiek> tmpList = new List<ICzłowiek>();
            tmpList.Add(k);
            tmpList.Add(m);
            return tmpList;
        }

        public static void consumerMethod(IList<ICzłowiek> tmpList)
        {
            foreach (var item in tmpList) 
            {
                item.Hi();
            }
        }

        static void Main(string[] args)
        {
        }
    }
}
